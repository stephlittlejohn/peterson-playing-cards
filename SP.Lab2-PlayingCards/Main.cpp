#include <iostream>
#include <conio.h>

using namespace std;

enum Rank : int
{
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE,
	JOKER = 10,
};

enum Suit
{
	DIAMOND,
	CLUB,
	SPADE,
	HEART,
	// Needed for the creation of the Joker card
	// Since The Joker is not associated with the 4 suits
	NONE
};

struct Card
{
	Rank rank;
	Suit suit;
};

// Function Prototypes
void PrintCard(Card card);

Card HighCard(Card card1, Card card2);

int main()
{
	Card c1, c2;
	
	// Example of Card 1 being highest
	c1.rank = ACE;
	c1.suit = CLUB;

	c2.rank = TWO;
	c2.suit = HEART;

	cout << "////////////// ROUND 1 //////////////\n";
	cout << "\nCard 1: ";
	PrintCard(c1);
	cout << "Card 2: ";
	PrintCard(c2);
	cout << "\nThe Winning Card: ";
	PrintCard(HighCard(c1, c2));

	// Example of Card 2 being highest
	c1.rank = THREE;
	c1.suit = SPADE;

	c2.rank = FIVE;
	c2.suit = DIAMOND;

	cout << "\n////////////// ROUND 2 //////////////\n";
	cout << "\nCard 1: ";
	PrintCard(c1);
	cout << "Card 2: ";
	PrintCard(c2);
	cout << "\nThe Winning Card: ";
	PrintCard(HighCard(c1, c2));

	// Example of a tie
	c1.rank = TEN;
	c1.suit = HEART;

	c2.rank = JOKER;
	c2.suit = NONE;

	cout << "\n////////////// ROUND 3 //////////////\n";
	cout << "\nCard 1: ";
	PrintCard(c1);
	cout << "Card 2: ";
	PrintCard(c2);
	cout << "\nThe Winning Card: ";
	PrintCard(HighCard(c1, c2));

	return 0;
}

// Functions
void PrintCard(Card card)
{
	if (card.suit != NONE)
	{
		if (card.rank <= 10) cout << card.rank;
		if (card.rank == JACK) cout << "Jack";
		if (card.rank == QUEEN) cout << "Queen";
		if (card.rank == KING) cout << "King";
		if (card.rank == ACE) cout << "Ace";
		cout << " of ";
		if (card.suit == SPADE) cout << "Spades";
		if (card.suit == HEART) cout << "Hearts";
		if (card.suit == DIAMOND) cout << "Diamonds";
		if (card.suit == CLUB) cout << "Clubs";
	}
	else 
	{
		cout << "The Joker";
	}
	cout << "\n";
}

Card HighCard(Card card1, Card card2)
{
	if (card1.rank > card2.rank) return card1;
	return card2;
}
